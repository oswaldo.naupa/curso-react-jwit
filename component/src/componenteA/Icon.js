import styled from 'styled-components';


const Container = styled.div`
  width: 25%;
`

const Body = styled.div`
    background-color: ${props => props.colorIcon};
    width: 100%;
    height: 100%;
    border-radius: 25px;
    display: flex;
    justify-content: center;
    align-items: center;
`

const Icon = styled.img`
    width: 45px;
`

const App = ({ icon, colorIcon }) => {
  return (
    <Container>
        <Body colorIcon={ colorIcon }>
           <Icon src={ icon } />
        </Body>
       
    </Container>
  )
}

export default App;
