import styled from 'styled-components';

import ComponentA from './componenteA';

import IconImage from './images/icon.png';

const Container = styled.div`
 
`

const App = () => {
  return (
    <Container>
      <ComponentA icon={ IconImage } colorIcon={ '#fff6a5'}/>
      <ComponentA icon={ IconImage } colorIcon={ '#EEFBF4'}/>
      <ComponentA icon={ IconImage } colorIcon={ '#F4F2FF'}/>
    </Container>
  )
}

export default App;
