import React from 'react';
import styled from 'styled-components';
import Menu from './Menu';

const Container = styled.div`
  //background-color: red;
`

const App = (): JSX.Element => {
  return (
    <Container>
         <Menu onAccept={e=> console.log(e)}
         onCancel={()=> console.log("seleccion cancelada")}/>
         <Menu options={[
           "PRICE LOW TO HIGH",
           "PRICE LOW TO LOW",
           "POPULARITY",
         ]} backgroundColor={"#21d0d0"}
         onAccept={e=> console.log(e)}
         onCancel={()=> console.log("seleccion cancelada")}/>
    </Container>
  )
}

export default App;
