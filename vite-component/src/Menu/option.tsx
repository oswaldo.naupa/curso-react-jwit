import React from 'react';
import styled from 'styled-components';

import Checkbox from "./checkbox";

import check from "../images/check-jwit.png";

const Container = styled.div`
   margin-left: 25px;
   margin-right: 25px;
   margin-top: 15px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
`

const Title = styled.h2`
    margin: 0px;
    color: white;
    font-weight: bold;
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
    font-size: 14px;
    text-transform: uppercase;
`


export interface params {
  value: string,
  state: boolean,
  onClick?: (params: { value:string, state: boolean}) => void
};

const App = (params: params): JSX.Element => {

  const handleClick= (state: boolean) => {
    
    if(typeof params.onClick === 'function') 
    params.onClick({value: params.value, state})
  }

  return (
    <Container>
        <Title>
            {params.value}
        </Title>
       <Checkbox image={ check } state={params.state}  onClick = {handleClick}/>
    </Container>
  )
}

export default App;