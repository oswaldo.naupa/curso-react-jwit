import styled from 'styled-components';
import { useState, useEffect } from 'react';


export const Container = styled.div<{visible: boolean}>`
    border-radius: 50%;
    border: 1.8px solid white;
    height: 35px;
    width: 35px;
    cursor: pointer;
    background-color: ${ props => props.visible ? 'white' : 'transparent'};
`

export const Icon = styled.img<{visible: boolean}>`
    border-radius: 50%;
    height: 35px;
    width: 35px;
    cursor: pointer;
    background-color: 'white';
    display: ${ props => props.visible ? 'block' : 'none'};
`

export interface params {
    image:string,
    onClick?: (value:boolean) => void;
    state: boolean;
  };

const App = (params: params): JSX.Element => {

 

    const handleClick= () => {

      if(typeof params.onClick === 'function') params.onClick(!params.state)
       
    }

    

  return (
    <Container visible={params.state} onClick={handleClick}>
        <Icon src={params.image} visible={params.state}/>
    </Container>
  )
}

App.defaultProps = {
  state: false,
}

export default App;