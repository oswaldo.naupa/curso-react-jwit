import styled from 'styled-components';

import check from '../images/check-jwit.png';

import cancel from '../images/cancel.png';

import reset from '../images/reload-icon.png';

import Button from './btn';


const Container = styled.div`
   display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: row;
    margin-top: 15px;
    padding: 25px 0px;
    border-top:1px solid #ffffff80;
`

const ContainerV2 = styled.div`
   display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: row;
`

interface options {
  icon: string,
  onClick: ()  =>  void,
}

export interface params {
  enableReset?: boolean,
  enableCancel?: boolean,
  
  onReset?: () => void,
  onAccept?: () => void,
  onCancel?: () => void,
} 


const App = (params:params): JSX.Element => {

  const handleReset = () => {
      console.log("vamos a resetear");

      if(typeof params.onReset === 'function') params.onReset()
  }

  const handleAcccept = () => {
      console.log("vamos a aceptar");

      if(typeof params.onAccept === 'function') params.onAccept()
  }

  const handleCancel = () => {
      console.log("vamos a cancelar");

      if(typeof params.onCancel === 'function') params.onCancel()
  }

  const data: options[] = [
    {icon: check, onClick: handleAcccept},
  ]
  if(params.enableReset) data.push( {icon: reset, onClick: handleReset},)
  if(params.enableCancel) data.push( {icon: cancel, onClick: handleCancel},)

  


  return (
    <Container>
    
      {
        data.map((v,i)=> <Button key={i} image={v.icon} onClick={v.onClick}/>)
      }


    </Container>
  )
}

App.defaultProps = {
  enableReset: true,
  enableCancel : true
}

export default App;
