import styled from 'styled-components';

import Option from './Option';

import Footer from './Footer';

const Container = styled.div`
    margin-top: 15px;
    background-color: ${props=> props.color};
    border-radius: 35px;
    //height: 200px;
    width: 400px;
    padding-top: 15px;
    //opacity: 0.8px;
`

const App = (params) => {

  /*
    color: string;
    options: string[];
  */

    const handleChange = (value) => {
      if(typeof params.onChange === 'function') params.onChange(value, params.id);
      console.log(value);
    }

    const handleReset= () => {
      if(typeof params.onReset === 'function') params.onReset(params.id);
    }

  return (
    <Container color={ params.color }>
        { 
            params.options.map((v,i)=> <Option key={i} {...v} onChange={handleChange}/>)
        }

        <Footer version= {params.footer} onReset={ handleReset}/>

    </Container>
  )
}

export default App;