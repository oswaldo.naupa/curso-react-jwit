import styled from 'styled-components';
import { useState, useEffect } from 'react';

import check from '../images/check-jwit.png';

const Container = styled.div`
   margin-left: 25px;
   margin-right: 25px;
   margin-top: 15px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
`

const Title = styled.h2`
    margin: 0px;
    color: white;
    font-weight: bold;
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
    font-size: 14px;
    text-transform: uppercase;
`

export const Circulo = styled.div`
    border-radius: 50%;
    border: 1.8px solid white;
    height: 35px;
    width: 35px;
    cursor: pointer;
    background-color: ${ props => props.visible ? 'white' : 'transparent'};
`

export const Icon = styled.img`
    border-radius: 50%;
    height: 35px;
    width: 35px;
    cursor: pointer;
    background-color: 'white';
    display: ${ props => props.visible ? 'block' : 'none'};
`

const App = ({ title, state, onChange }) => {

    const [visible, setVisible] = useState(false);

    /*useEffect(() => {
        if(state) setVisible(true);
        else setVisible(false);
    }, [state])*/

    const handleChange = () => {
        if(typeof onChange === 'function') onChange(title)
    }
    

  return (
    <Container>
        <Title>
            {title}
        </Title>
        <Circulo visible={state} onClick={handleChange}>
            <Icon src={check} visible={state}/>
        </Circulo>
    </Container>
  )
}

export default App;