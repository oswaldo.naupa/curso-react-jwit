import styled from 'styled-components';

import { Circulo, Icon } from './Option';

import check from '../images/check-jwit.png';

import cancel from '../images/cancel.png';

import reset from '../images/reload-icon.png';
import { useState } from 'react';

const ContainerV1 = styled.div`
   display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: row;
    margin-top: 15px;
    padding: 25px 0px;
    border-top:1px solid #ffffff80;
`

const ContainerV2 = styled.div`
   display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: row;
`

const Item = ({v, onClick}) => {

    return (
        <Circulo visible={true} style={{ height:60, width:60, margin: 6 }} onClick={onClick}>
            <Icon  visible={true} src={ v } style={{ height:60, width:60, boxShadow: 'rgba(50, 50, 93, 0.25) 0px 2px 5px -1px, rgba(0, 0, 0, 0.3) 0px 1px 3px -1px' }}/>
        </Circulo>
    )
}

const App = ({version, onReset, onCancel, onAccept }) => {

    const handleReset = () => {
        console.log("vamos a resetear");

        if(typeof onReset === 'function') onReset()
    }

    const handleOk = () => {
        console.log("vamos a aceptar");

        if(typeof onAccept === 'function') onAccept()
    }

    const handleCancel = () => {
        console.log("vamos a cancelar");

        if(typeof onCancel === 'function') onCancel()
    }

    

    if(version === 1){

        let data = [
            {icon: cancel, onClick: handleCancel},
            {icon: reset, onClick: handleReset},
            {icon: check, onClick: handleOk},
        ]
        return (

            <ContainerV1>
    
              {
                data.map((v,i)=> <Item v= {v.icon} onClick={v.onClick}/>)
              }
    
    
            </ContainerV1>
      )
    }

    if(version === 2){

        let data = [
            {icon: cancel, onClick: handleCancel},
           
            {icon: check, onClick: handleOk},
        ]
        return (

            <ContainerV2>
    
              {
                data.map((v,i)=> <Item v= {v.icon} onClick={v.onClick}/>)
              }
    
    
            </ContainerV2>
      )
    }
    return <></>;
  
}

export default App;