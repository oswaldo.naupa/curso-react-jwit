import { useEffect, useState } from 'react';
import styled from 'styled-components';
import Card from './Card';
import Submenu from './submenu';

const Container = styled.div`
 
`

const App = () => {

  const [data, setData] = useState([
    {
      id: 1,
      color: "#21d0d0",
      options : [
        { title: "PRICE LOW TO HIGH", state: false},
        { title: "PRICE LOW TO LOW", state: false},
        { title: "POPULARITY", state: false},
      ],
      footer:2
    },
    {
      id: 2,
      color: "#F06F37",
      options : [
        { title: "1up nutririon", state: false},
        { title: "asitis", state: false},
        { title: "avvatar", state: false},
        { title: "big muscles", state: false},
        { title: "bpi sports", state: false},
        { title: "bsn", state: false},
        { title: "cellucor", state: false},
        { title: "domin8r", state: false},
      ],
      footer: 1
    },
  ])

  const dataPreview = [
    {
      id: 1,
      color: "#21d0d0",
      options : [
        { title: "PRICE LOW TO HIGH", state: false},
        { title: "PRICE LOW TO LOW", state: false},
        { title: "POPULARITY", state: false},
      ],
      footer:2
    },
    {
      id: 2,
      color: "#F06F37",
      options : [
        { title: "1up nutririon", state: false},
        { title: "asitis", state: false},
        { title: "avvatar", state: false},
        { title: "big muscles", state: false},
        { title: "bpi sports", state: false},
        { title: "bsn", state: false},
        { title: "cellucor", state: false},
        { title: "domin8r", state: false},
      ],
      footer: 1
    },
  ]

  const handleChange = (value, id) => {
    console.log(value);
    console.log(id);

    const match = data.find(el => el.id === id);

   
    const act = match.options.find(el => el.title === value);

    const copy = [...match.options];

    const targetIndex = copy.findIndex(f=> f.title === value );

    if(targetIndex > -1){
      copy[targetIndex] = { title: value, state: !act.state};

      match.options = copy;
    }


    const copyList = [...data];

    const targetIndexList = copyList.findIndex(f=> f.id=== id );

    if(targetIndexList > -1){
      copyList[targetIndexList] = match;
    }

    console.log(copyList);

    setData(copyList)

  }

  const handleReset = (id) => {
    const copy = [...data];

    const targetIndex = copy.findIndex(f=> f.id=== id );

    if(targetIndex > -1){

      var raw = [];

      const options = data.find(el => el.id === id).options;

      for(let x of options) raw.push({title: x.title, state: false})

      copy[targetIndex].options = raw

    }

    //console.log(copyList);

    setData(copy)
  }
  

  return (
    <Container>
        <Card/>
        {
          data.map((v,i)=> <Submenu key={i} {...v} onChange={handleChange} onReset= {handleReset}/>)
        }

    </Container>
  )
}

export default App;