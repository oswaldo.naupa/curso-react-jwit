## Installation & Usage

```sh
npm install @react/menu --save
```

### Include the Component

```js
import App from '@react/menu'

const App = () => {
    return <App />
}
```