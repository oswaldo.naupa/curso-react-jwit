import React from "react";
import styled from 'styled-components';


export const Container = styled.div`
    border-radius: 50%;
    border: 1.8px solid white;
    cursor: pointer;
    background-color: white;
    height:60px; width:60px; margin: 6px;
`

export const Icon = styled.img`
    border-radius: 50%;
    height: 35px;
    width: 35px;
    cursor: pointer;
    background-color: white;
    height:60px;
    width:60px; 
    box-shadow: rgba(50, 50, 93, 0.25) 0px 2px 5px -1px, rgba(0, 0, 0, 0.3) 0px 1px 3px -1px;
   
`

export interface params {
    image:string,
    onClick?: () => void
  };

const App = (params: params): JSX.Element => {

    const handleClick= () => {

      if(typeof params.onClick === 'function') params.onClick()
       
      }
    

  return (
    <Container  onClick={handleClick}>
        <Icon src={params.image} />
    </Container>
  )
}

export default App;