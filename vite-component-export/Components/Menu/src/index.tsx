import React from 'react';
import styled from 'styled-components';
import Option, { params as paramsComponentOption} from './option';
import Footer from './footer';
import { useState, useEffect } from 'react';
import { resources } from './types';


const Container = styled.div`
    margin-top: 15px;
    background-color: ${props=> props.color};
    border-radius: 35px;
    //height: 200px;
    width: 400px;
    padding-top: 15px;
`

export interface option {
  value:string;
  state: boolean;
}

export interface params {
  options: string[];
  backgroundColor: string;
  onAccept?: (options: option[]) => void;
  onCancel?: () => void;

  resources: resources;
}

const App = (params:params): JSX.Element => {

  const [options, setOptions] = useState<option[]>([])

  useEffect(() => {
    var raw:option[] = [];

    params.options.forEach(el=> raw.push({ value: el, state: false }))

    setOptions(raw)
  }, [params.options])
  

const handleClick:paramsComponentOption['onClick'] = (e) => {

    const copy: option[] = [...options];
    const index: number = copy.findIndex(f=> f.value === e.value );

    if(index > -1) copy[index] = e;

    setOptions(copy);

    console.log(copy);
    
} 

const handleAccept = () => {

  if(typeof params.onAccept === 'function') params.onAccept(options)

}

const handleReset = () => {

  if(typeof params.onAccept === 'function') params.onAccept(options)

  var raw:option[] = [];

  params.options.forEach(el=> raw.push({ value: el, state: false }))

  setOptions(raw)

  console.log(options);
  

}
const handleCancel = () => {

  if(typeof params.onCancel === 'function') params.onCancel()

}


  return (
    <Container color={params.backgroundColor}>
       {options.map((option,i) => <Option key={i} value={option.value} image={params.resources.check}state={option.state} onClick={handleClick}/>)}
        <Footer onAccept={handleAccept} onReset={handleReset} onCancel={handleCancel} resources={params.resources}/>
    </Container>
  )
}

App.defaultProps = {
  options: [
    "1up nutririon",
    "asitis",
    "avvatar",
    "big muscles",
    "bpi sports",
    "bsn",
    "cellucor",
    "domin8r",
  ],
  backgroundColor: "#F06F37"
}

export default App;
