import React from 'react';
import styled from 'styled-components';
import Menu from '@react/menu';
import check from './images/check-jwit.png';

import cancel from './images/cancel.png';

import reset from './images/reload-icon.png';

const Container = styled.div`
  //background-color: red;
`

const App = (): JSX.Element => {
  return (
    <Container>
         <Menu resources={ {check, cancel, reset} } onAccept={e=> console.log(e)}
         onCancel={()=> console.log("seleccion cancelada")}/>
         <Menu resources={ {check, cancel, reset} } options={[
           "PRICE LOW TO HIGH",
           "PRICE LOW TO LOW",
           "POPULARITY",
         ]} backgroundColor={"#21d0d0"}
         onAccept={e=> console.log(e)}
         onCancel={()=> console.log("seleccion cancelada")}/>
    </Container>
  )
}

export default App;
